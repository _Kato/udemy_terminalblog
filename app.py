from database import Database
from models.post import Post

__author__ = '_Kato'

Database.initialize()

blog = Blog(author="Kato",
            title="XDDD Sample",
            description="Sample Description")
blog.new_post()

blog.save_to_mongo()

Blog.from_mongo()

blog.get_posts() # Post.from blog(id)
